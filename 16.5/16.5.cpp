﻿#include <iostream>
#include <locale.h>
#include <time.h>

int main()
{
	setlocale(LC_ALL, "Russian");
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int a, data, n, summ = 0;
	data = buf.tm_mday;

	std::cout << "Введи размерность массива" << std::endl;
	std::cin >> a;

	int** array;
	array = new int* [a+1];

	for (int i = 0; i < a+1; i++)
	{
		array[i] = new int[a+1];
	}

	for (int i = 0; i < a+1; i++)
	{
		for (int j = 0; j < a+1; j++)
		{
			array[i][j] = (i + j);
			std::cout << array[i][j] << " ";
		}
		std::cout << std::endl;
	}

	std::cout << "Введи число N" << std::endl;
	std::cin >> n;
	for (int i=0; i < a+1; i++) summ += array[i][data % n];
	std::cout << "Сумма чисел по индексу " << data % n << " = " << summ << std::endl;
}